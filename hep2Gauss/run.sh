export APPCONFIGOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r395/options

lb-run -c best Gauss/v49r18 gaudirun.py ${APPCONFIGOPTS}/Gauss/Beam6500GeV-md100-2018-nu1.6.py ${APPCONFIGOPTS}/Gauss/DataType-2018.py gauss.py | tee log

