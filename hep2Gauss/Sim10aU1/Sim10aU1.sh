export APPCONFIGOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r408/options

lb-set-platform x86_64_v2-centos7-gcc11-opt
lb-run Gauss/v55r4 gaudirun.py ${APPCONFIGOPTS}/Gauss/Beam7000GeV-md100-nu7.6-HorExtAngle.py ${APPCONFIGOPTS}/Gauss/EnableSpillover-25ns.py ${APPCONFIGOPTS}/Gauss/Gauss-Upgrade-Baseline-20150522.py ${APPCONFIGOPTS}/Gauss/G4PL_FTFP_BERT_EmNoCuts.py gauss.py | tee log_Gauss

export APPCONFIGOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r404/options

lb-set-platform x86_64_v2-centos7-gcc10-opt
lb-run Boole/v43r0 gaudirun.py ${APPCONFIGOPTS}/Boole/Default.py ${APPCONFIGOPTS}/Boole/EnableSpillover.py ${APPCONFIGOPTS}/Boole/Boole-Upgrade-Baseline-20200616.py ${APPCONFIGOPTS}/Boole/Upgrade-RichMaPMT-NoSpilloverDigi.py ${APPCONFIGOPTS}/Boole/xdigi.py ${APPCONFIGOPTS}/Boole/Boole-Upgrade-IntegratedLumi.py boole.py | tee log_Boole

