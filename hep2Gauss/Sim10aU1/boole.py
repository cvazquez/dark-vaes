from Configurables import LHCbApp

LHCbApp().DDDBtag    = "dddb-20210617"
LHCbApp().CondDBtag  = "sim-20210617-vc-mu100"

from GaudiConf import IOHelper
data = ['Gauss.sim']
IOHelper('ROOT').inputFiles(data)

from Configurables import Boole
Boole().DatasetName = "Boole"
