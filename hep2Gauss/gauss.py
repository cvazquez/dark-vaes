from Gaudi.Configuration import *
from Gauss.Configuration import *
from Configurables import Generation, Special, FixedNInteractions, ReadHepMCAsciiFile, LHCbApp, OutputStream
from GaudiKernel import SystemOfUnits

Generation().PileUpTool = "FixedNInteractions"
Generation().addTool( FixedNInteractions )
Generation().FixedNInteractions.NInteractions = 1

Generation().SampleGenerationTool = "Special"
Generation().addTool( Special )
Generation().Special.CutTool = ""
Generation().DecayTool = ""
Generation().Special.ProductionTool = "ReadHepMCAsciiFile/ReadHepMC"
Generation().Special.addTool( ReadHepMCAsciiFile, name="ReadHepMC" )
Generation().Special.ReadHepMC.Input = 'mMed-125_mDark-0.5_temp-0.25_decay-generic.hepmc'

Generation().Special.ReadHepMC.OutputLevel = DEBUG

Gauss().OutputType = 'XGEN'
OutputStream('GaussTape').Output = "DATAFILE='Gauss.xgen' TYP='POOL_ROOTTREE' OPT='RECREATE'"
LHCbApp().EvtMax   = 1
LHCbApp().DDDBtag    = "dddb-20170721-3"
LHCbApp().CondDBtag  = "sim-20190430-vc-md100"
