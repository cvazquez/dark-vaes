import numpy as np
import pandas as pd
import statistics
import matplotlib.pyplot as plt
from plot_hep_mc_csv import SUEP 

#All this packages are necessary to the correct work of the file. The "from plot_hep_mc_csv import SUEP" is the most important part,
#as the whole treatment of the data depends on this file, which has to be in the same directory of the .py file.
        
name='mMed-125_mDark-0.5_temp-0.5_decay-generic_10kDM.hepmc'
print(name)
suep=SUEP(name) #I Load the simulation
suep.doTest() #I process it
suep.processEvents() #I obtain all the variables to plot
columnsfinal=["Nevents","trackPt","trackEta","trackPhi","trackPx","trackPy","trackPz","trackE","nTracks"] #Columns of the dataframe
ptEv=[]
etaEv=[]
phiEv=[]
pxEv=[]
pyEv=[]
pzEv=[]
EEv=[]
ScalarMEv=[]
ptEsc=[]
etaEsc=[]
phiEsc=[]
Nevents=[]
Tracks=[]
a=10000 #Number of events per simulations
patata=0 #counter of tracks
for k in range(a):
    b=suep.nTracks[k]
    if b!=0: # If the number of tracks of the event is higher than 0
        for j in range(b):
            Nevents.append(k+1)
            #ptEsc.append(suep.scalarPt[k])
            #etaEsc.append(suep.scalarEta[k])
            #phiEsc.append(suep.scalarPhi[k])
            #ScalarMEv.append(suep.scalarM[k])
            ptEv.append(suep.trackPt[patata])
            etaEv.append(suep.trackEta[patata])
            phiEv.append(suep.trackPhi[patata])
            pxEv.append(suep.trackPx[patata])
            pyEv.append(suep.trackPy[patata])
            pzEv.append(suep.trackPz[patata])
            EEv.append(suep.trackE[patata])
            Tracks.append(suep.nTracks[k])
            patata+=1 #I change from track and keep counting
        print('Hemos cambiado de evento con', b, 'tracks')
    elif b==0 and k==a-1: #This is an excepcion case for the last event
        ptEv.append(0.)
        etaEv.append(0.)
        phiEv.append(0.)
        #pxEv.append(0.)
        #pyEv.append(0.)
        pzEv.append(0.)
        EEv.append(0.)
        Tracks.append(0.)

variablesfinales=[]
variablesfinales.append(Nevents)
variablesfinales.append(ptEv)
variablesfinales.append(etaEv)
variablesfinales.append(phiEv)
variablesfinales.append(pxEv)
variablesfinales.append(pyEv)
variablesfinales.append(pzEv)
variablesfinales.append(EEv)
variablesfinales.append(Tracks)
       
df_esc=pd.DataFrame(variablesfinales).transpose() #I create the pandas dataframe
df_esc.columns=columnsfinal
print(df_esc)
namedf="SUEP_Simulation_10kDM_eta11.csv"
df_esc.to_csv(namedf)


