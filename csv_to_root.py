from ROOT import *
from array import array

### update me ###
name     = 'mMed-125_mD-1.25_TD-3.0_final'
nametree = 'tree'
path     = './'
#################

branches = ['index', 'evtNumber', 'trackPt', 'trackEta', 'trackPhi', 'trackPx', 'trackPy', 'trackPz', 'trackE', 'trackX', 'trackY', 'trackZ', 'Isotropy', 'scalarPt', 'scalarEta', 'scalarPhi', 'scalarM', 'nTracks', 'ID']
arrays   = {}
for branch in branches: arrays[branch] = array('f', [0.])

f = TFile(name+'.root', 'recreate')
t = TTree(nametree, nametree)
for branch in branches: t.Branch(branch, arrays[branch], branch+'/F')

csv = open(path+name+'.csv')
doc = csv.readlines()
del doc[0] # remove the header
for line in doc:
  line = line.split(',')
  idcounter = 0
  for j in line: 
    branch = branches[idcounter]
    arrays[branch][0] = float(j)
    idcounter += 1
  t.Fill()

t.Write()
f.Close()
csv.close()

