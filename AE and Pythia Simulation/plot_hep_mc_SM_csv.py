#!/usr/bin/env python
# based Katherine Pachal & Jess Nelson's skeleton 
# https://gitlab.cern.ch/snowmass-track-trigger/scripts_lhe_hepmc
# event displays from Chris Papageorgakis

from particle import Particle
import pyhepmc_ng as hep
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import math

def getStatusOne(particles):
    # get all status 1 decay products
    statusOne = []

    for particle in particles: 
      if particle.status != 1 : continue
      statusOne.append(particle)

    return statusOne

def hasScalarParent(particle):
    # returns True if a particle descends from scalar 
    scalar_parent=False

    parents=particle.parents
    if not parents : return scalar_parent
    else:
        for parent in particle.parents:
            if parent.pid == 999999 :return True# dark meson
            else : return hasScalarParent(parent)

    return statusOne

def isCharged(particle):
    part = Particle.from_pdgid(particle.pid)
    if part.charge !=0 : return 1
    else : return 0

def getIsotropy(particles,nSteps=100):
    # compute event isotropy, based on.... 
    # https://github.com/cms-sw/cmssw/blob/master/PhysicsTools/CandUtils/src/EventShapeVariables.cc

    def getSum(phi,particle):
        cosphi, sinphi = math.cos(phi), math.sin(phi)
        return math.fabs( cosphi * particle + sinphi * particle )

    phis = np.linspace(0,2*math.pi,nSteps)
    cosphis = np.cos(phis)
    sinphis = np.sin(phis)
    sums = np.zeros(nSteps)
    for particle in particles:
        sums += np.abs( cosphis * particle.momentum.x + sinphis * particle.momentum.y)

    eOut = min(sums)
    eIn = max(sums)

    return 1.-(eIn - eOut)/eIn


class SUEP():

    def __init__(self, infile):
        #
        # Get mediator mass, dark meson mass, temp, etc from filename
        # 
        self.infile = infile


        # Configurable options for running 
        self.verbose = False
        self.maxEvents = 500000	
        self.trackPtCut = 0.250
        self.maxEtaCut = 4.5
        self.minEtaCut = 2.0
        self.maxPtCut= 75.0 

        #
        # Used for plotting, add what you like
        # 
        self.scalarPt  = []
        self.scalarEta = []
        self.scalarPhi = []
        self.scalarM   = []

        self.nCharged   = [] 
        self.chargedPt  = []

        self.nTracks    = []
        self.trackPt     = []
        self.trackEta    = []
        self.trackPhi    = []
        self.trackPx     = []
        self.trackPy    = []
        self.trackPz    = []
        self.trackE = []   
        self.trackx=[]
        self.tracky=[]
        self.trackz=[]
        self.isotropy   = []
        self.ht   = []


    def getScalarInfo(self,particles):
        # Find the mediator
        for particle in particles :
            
            # Find the scalar
            if (particle.pid != 25 ) : continue 
            if (particle.status != 62 ) : continue

            # Get the scalar four vector

            self.scalarPt .append(particle.momentum.pt()) 
            self.scalarEta.append(particle.momentum.eta())
            self.scalarPhi.append(particle.momentum.phi())
            self.scalarM  .append(particle.momentum.m())
            
            break

        return

    def saveTrackInfo(self,particles):

        evt_ht = 0 # scalar sum track pt
        evt_ncharged = 0 
        evt_ntracks = 0
        evt_tracks = []
        evt_trackEta = []
        evt_trackPhi = []
        evt_trackPt  = []
        evt_trackPx = []
        evt_trackPy = []
        evt_trackPz = []
        evt_trackE = []
        evt_trackx=[]

        for particle in getStatusOne(particles):

            # only charged particles
            if not isCharged(particle) : continue 

            evt_ncharged += 1
            self.chargedPt.append(particle.momentum.pt())
        
            # charged particles in tracking acceptance
            if particle.momentum.pt() < self.trackPtCut : continue
            if particle.momentum.eta() > self.maxEtaCut : continue
            if particle.momentum.eta() < self.minEtaCut : continue
            if particle.momentum.pt() > self.maxPtCut : continue
            
            evt_ntracks += 1
            evt_tracks.append(particle)
            evt_ht += particle.momentum.pt()
            #a=particle.production_vertex
            #self.trackx.append(a.position.x)
            #self.tracky.append(a.position.y)
            #self.trackz.append(a.position.z)
            self.trackPt.append(particle.momentum.pt())
            self.trackEta.append(particle.momentum.eta())
            self.trackPhi.append(particle.momentum.phi())
            self.trackPx.append(particle.momentum.px)
            self.trackPy.append(particle.momentum.py)
            self.trackPz.append(particle.momentum.pz)
            self.trackE.append(particle.momentum.e)
        # for event displays
       # self.trackPt .append(evt_trackPt)
       # self.trackEta.append(evt_trackEta)
       # self.trackPhi.append(evt_trackPhi)
       # self.trackPx.append(evt_trackPx)
       # self.trackPy.append(evt_trackPy)
       # self.trackPz.append(evt_trackPz)
       # self.trackE.append(evt_trackE)

        
        # Event level quantities 
        self.nCharged.append(evt_ncharged)
        self.nTracks .append(evt_ntracks)
        self.ht.append(evt_ht)

        return 

    def processEvents(self):
        
        print( "Processing {}".format(self.infile) )

        # Reads the file
        with hep.open(self.infile) as f:
          # Just keeps looping
          while True :
        
            # Try to get an event
            evt = f.read()
            if not evt : break # we're at the end of the file
        
            # Stop if we're past max events
            if evt.event_number >= self.maxEvents : 
                break
        
            # Study the mediator and final state charged particles 
            self.getScalarInfo(evt.particles)
            # End looping because we found the scalar
            self.saveTrackInfo(evt.particles)

            if self.verbose : 
                print("Event",evt.event_number)

                # Status=1 particles
                print("Some final state charged particles: ")
                particles = getStatusOne(evt.particles)
                i=0
                for part in particles : 
                  #if not hasScalarParent(part): continue
                  if part.momentum.pt() < 0.250: continue 
                  if not isCharged(part): continue
                  i+=1
                  trk=part.momentum
                  print("pdgID {} : PtEtaPhiM[{:.2f}, {:.2f}, {:.2f}, {:.2f}]".format(part.pid,trk.pt(),trk.eta(),trk.phi(),trk.m()))
                  if i>15: break
                # Event level info
                print("Number of status=1 particles:",len(particles)) 
                print("Number of charged particles:",self.nCharged[-1]) 
                print("Number of truth tracks:",self.nTracks[-1]) 
                

        return

    def doTest(self):
        self.verbose=True
        self.maxEvents=1
        self.processEvents()
        self.verbose=False
        self.maxEvents=500000
        return
    
   
