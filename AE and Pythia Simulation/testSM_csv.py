#from plot_hep_mc import SUEP
import numpy as np
import pandas as pd
import statistics
import matplotlib.pyplot as plt
from plot_hep_mc_SM_csv import SUEP
        
name='SMSimulation_500000.hepmc'
print(name)
suep=SUEP(name) #I Load the simulation
suep.doTest() #I process it
suep.processEvents() #I obtain all the variables to plot
columnsname_esc=["scalarPt","scalarEta","scalarPhi","scalarM","nCharged","nTracks","isotropy","ht"]
columnsname_not=["chargedPt"]
columnstrack=["trackPt","trackEta","trackPhi"]
columnsfinal=["Nevents","trackPt","trackEta","trackPhi","trackPx","trackPy","trackPz","trackE","nTracks"]
ptEv=[]
etaEv=[]
phiEv=[]
pxEv=[]
pyEv=[]
pzEv=[]
EEv=[]
ScalarMEv=[]
ptEsc=[]
etaEsc=[]
phiEsc=[]
Nevents=[]
nTracks=[]
a=500000 #Numero de eventos inicial (sin eliminar)
patata=0
for k in range(a):
    b=suep.nTracks[k] #Numero de tracks por evento
    if b==0 and k!=a-1:
        patata+=1
    elif b!=0:
        for l in range(b):
            Nevents.append(k+1-patata)
            #ptEsc.append(suep.scalarPt[k]) No hay mediador escalar, obvio que no funcione
            #etaEsc.append(suep.scalarEta[k])
            #phiEsc.append(suep.scalarPhi[k])
            #ScalarMEv.append(suep.scalarM[k])
            ptEv.append(suep.trackPt[l])
            etaEv.append(suep.trackEta[l])
            phiEv.append(suep.trackPhi[l])
            pxEv.append(suep.trackPt[l])
            pyEv.append(suep.trackEta[l])
            pzEv.append(suep.trackPhi[l])
            EEv.append(suep.trackE[l])
            nTracks.append(b)
    elif b==0 and k==a-1:
        Nevents.append(a-patata)
        ptEv.append(0.)
        etaEv.append(0.)
        phiEv.append(0)
        pxEv.append(0)
        pyEv.append(0)
        pzEv.append(0)
        EEv.append(0)
        nTracks.append(0)  
variablesfinales=[]
variablesfinales.append(Nevents)
variablesfinales.append(ptEv)
variablesfinales.append(etaEv)
variablesfinales.append(phiEv)
variablesfinales.append(pxEv)
variablesfinales.append(pyEv)
variablesfinales.append(pzEv)
variablesfinales.append(EEv)
#variablesfinales.append(suep.scalarPt)
#variablesfinales.append(suep.scalarEta)
#variablesfinales.append(suep.scalarPhi)
#variablesfinales.append(suep.scalarM)
variablesfinales.append(nTracks)
       
df_esc=pd.DataFrame(variablesfinales).transpose() #I create the pandas dataframe
df_esc.columns=columnsfinal
print(df_esc)
namedf="SMSimulation_500000.csv"
df_esc.to_csv(namedf)
        #matrix[temps.index(i),mass.index(j)]=statistics.median(variables_esc[0])

